// console.log('Hello')

// ES6


	// -latest version of writing JS and in fact is one of the  major update to JS
	// -let, consr - are ES6 update, these are the new standerds of creating variables



// Exponent Operator(**)

const firstNum = 8 ** 2;
console.log(firstNum);

const secondNum = Math.pow(8,2);
console.log(secondNum);


let string1 = 'fun';
let string2 = 'Bootcamp';
let string3 = 'Coding';
let string4 = 'Javascript';
let string5 = 'Zuitt';
let string6 = 'Learning';
let string7 = 'love';
let string8 = 'I';
let string9 = 'is';
let string10 = 'in';

// mini activity

	// let sentence1 = string8 + ' ' + string7 +  ' ' + string5 + ' ' + string2;
	// console.log(sentence1);
	// let sentence2 = string3 + ' ' + string9 + ' ' + string1;	
	// console.log(sentence2);

// Template Literals
/*
	-allow us to create string using ( `` ) and easily embed JS expression in it
	-it allows us to write strings without using the concatenation operator(+)
	-greatly helps with code readability
*/	
// "", '', - string literals

let sentence1 = `${string8} ${string7} ${string5} ${string2}!`;
console.log(sentence1);

// ${} is a placeholder that is used to embed JS expressions when creating strings using template literals


let name = 'John';

// Pre-Template literal string
// ("") or ('')

// let message = 'Hello' + name + '! Welcome to programming!';

// Strings using template literals
// (``) backticks

message = `Hello ${name}! Welcome to programming.`
console.log(message);

// Multi-line using template literals

const anotherMessage = ` ${name} attended a math competition.
He won it by solving the problem 8 ** 2 with the solution of ${firstNum}`
console.log(anotherMessage);

let dev = {
name: 'Peter',
lastName: 'Parker',
occupation: 'developer',
income: 50000,
expenses: 60000

}
console.log(`${dev.name} is a ${dev.occupation}.`);
console.log(`His income is ${dev.income} and expenses at ${dev.expenses}. His current balance is ${dev.income - dev.expenses}.`);

const interestRate = .1;
const principal = 505023;

console.log(`The interest on your savings account is ${principal * interestRate}`);

// Array Destructuring
/*
	-allows to unpack elements in array into distinct variables

	-Syntax:

	let/const [variableNameA, variableNameB, variableNameC] = array;

*/

const fullName = ['Juan', 'Dela', 'Cruz', 'Y', 'Rizal'];

// Pre-Array Destructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! it is nice to meet you`);

// Array Destructuring

const[firstName, middleName, lastName,,anotherName] = fullName;
console.log(firstName);
console.log(middleName);
console.log(lastName);

console.log(`Hello ${firstName} ${middleName} ${lastName} ${anotherName}! it is nice to meet you`);

// Object Destructuring
/*
	-allows us to unpack properties of objects into distinct variables
	-Syntax:
	let/const {propertyNameA, propertyNameB, propertyNameC} = Object
*/
const person = {
givenName: 'Jane',
maidenName: 'Dela',
familyName: 'Cruz'
};

// Pre-Object Destructuring
/*
	-access them using . or []
*/

console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

// Object Destructuring


const {givenName, maidenName, familyName} = person;
console.log(givenName);
console.log(maidenName);
console.log(familyName);


// Arrow Functions
/*
	-Compact alternative syntax to traditional functions

*/

const hello = () => {
	console.log('Hello World')
}

// const hello = function hello(){
// 	console.log('Hello world');
// }

hello();

// Pre-Arrow Function and template literals
/*
	-Syntax:
		function functionName(parameterA, parameterB, parameterC){
		console.log();
		}
*/


// Arrow Function
/*
	Syntax:
		let/const variableName = (parameterA, parameterB, parameterC) => {
			console.log;

		}
*/

const printFullName = (firstName, middleInitial, lastName) => {
	console.log(`${firstName} ${middleInitial} ${lastName}`)
}
printFullName('John', 'D', 'Smith');

// Arrow Functions with loops
// Pre-arrow function

const students = ['John', 'Jane', 'Judy'];


students.forEach(function(student){
	console.log(`${student} is a student.`);
})


// Arrow Function

students.forEach((student) => {
	console.log(`${student} is a student.`)
})
// student.forEach((student)=>console.log(student ' is a student.'));

// Implicity Return Statement
/*
	-there are instances when you can omit the "return" statement
	-this works because even without the "return" statement JS IMPLICITLY adds it for the result of the function
*/

// Pre-Arrow Function

// const add = (x,y) => {
// 		return x+y;
// 	}

	// {} in an arrow function are code blocks. if an arrow function has a {} or code block, we're going to need to use a return

	// implicit return will only work on arrow functions without {}

	// const add = (x,y)=> x+y;
	// let total = add(1,2);
	// console.log(total);


	const subtract = (x,z) => (x-z);
	const multiply = (x,z) => (x*z);
	const divide = (x,z) => (x/z);
	let answer1 = subtract(1,2);
	let answer2 = multiply(1,2);
	let answer3 = divide(1,2);
	console.log(answer1);
	console.log(answer2);
	console.log(answer3);


	// Default Function Argument Value
	/*
		-provide a default argument value if none is provided when the function is invoked
	*/

	const greet = (name = 'User') => {
		return `Good evening, ${name}`;
	}
	console.log(greet());
	console.log(greet('John'));

	// Class-Based Object Blueprint
	/*
		-allows creation/instantation of objuects using classes as blueprint
	*/

	// Creating a class
	/*
		-the constructor is a special method of a class for creating/initializing an object for that class
		-Syntax:

			class classname {
				constructor(objectPropertyA, objectPropertyB){
				this.objectPropertyA = objectPropertyA;
				this.objectPropertyB = objectPropertyB;
				}
			}
	*/

	class Car {
		constructor(brand,name,year){
			this.brand = brand;
			this.name = name;
			this.year = year;
		}
	}

	//Instantiating an object

	/*
		-the "new" operator creates/instantiate a new object with the given arguments as the value of its properties
		-Syntax:
			let/const variableName = new className();
	*/

	/*
		-creating a constant with the "const" keyword and assigning it a value of an object makes it so we can't reassign it with another data type
	*/

	const myCar = new Car();

	console.log(myCar);

	myCar.brand = 'Ford';
	myCar.name = 'Everest';
	myCar.year = 1996;

	console.log(myCar);

	const myNewCar = new Car('Toyota', 'Vios', 2021);
	console.log(myNewCar);


	class Character {
		constructor(name,role,strength,weakness){
			this.name = name;
			this.role = role;
			this.strength = strength;
			this.weakness = weakness;
		}

	}

	const myCharacter = new Character ();
	myCharacter.name = 'Goku';
	myCharacter.role = 'Point Guard';
	myCharacter.strength = 'super saiyan';
	myCharacter.weakness = 'Berus';
	console.log(myCharacter);

	const myNewCharacter = new Character('Eugene', 'Center', 'Reigan', 'team fight');
	console.log(myNewCharacter);

