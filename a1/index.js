console.log('World');

const getCube = 2 ** 3;
console.log(`The cube of 2 is ${getCube}`);



	let houseNumber = 258;
	let street = 'Washington Ave';
	let city = 'NW';
	let state = 'California';
	let zip = 9001

	// 'I live at 258 Washington Ave NW, California 9001'

let address = `I live at ${houseNumber} ${street} ${city} ${state} ${zip}`;
console.log(address);

let animal = {
	name: 'Lolong',
	type: 'crocodile',
	weight: 1075,
	measurement: '20 ft 3 in'

}
console.log(`${animal.name} was a saltwater ${animal.type}. He weighed at  ${animal.weight} kgs with a measurement of ${animal.measurement}`);

let num = [1,2,3,4,5];

num.forEach((number) => {
		console.log(number);

})

let reduceNumber = num.reduce((total, num) => total + num);
console.log(reduceNumber);


class Dog {
		constructor(name,age,breed,color){
			this.name = name;
			this.age = age;
			this.color = color;
			this.breed = breed;

		}
	}
	const myDog1 = new Dog('Luna', '7 months', 'Yellow', 'Labrador');
	console.log(myDog1);
	const myDog2 = new Dog('cooper', '1 and 9 months', 'Liver', 'Labrador');
	console.log(myDog2);